// classes.js

// Clase que representa al jugador
class Player {
    constructor(x, y, width, height, color) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color;
        this.speed = 50; // Velocidad de movimiento del jugador
    }

    draw(ctx) {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }

    move(direction, canvas) {
        switch (direction) {
            case 'left':
                this.x -= this.speed;
                if (this.x < 0) this.x = 0; // Limita el movimiento a la izquierda
                break;
            case 'right':
                this.x += this.speed;
                if (this.x + this.width > canvas.width) this.x = canvas.width - this.width; // Limita el movimiento a la derecha
                break;
            case 'up':
                this.y -= this.speed;
                if (this.y < 0) this.y = 0; // Limita el movimiento hacia arriba
                break;
            case 'down':
                this.y += this.speed;
                if (this.y + this.height > canvas.height) this.y = canvas.height - this.height; // Limita el movimiento hacia abajo
                break;
        }
    }
}

// Clase que representa a un enemigo
class Enemy {
    constructor(x, y, width, height, color) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color;
        this.speed = 2; // Velocidad de movimiento del enemigo
    }

    draw(ctx) {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }

    update(canvas) {
        this.y += this.speed;
        if (this.y > canvas.height) {
            this.y = -this.height;
            this.x = Math.random() * (canvas.width - this.width);
        }
    }
}

// Clase que representa un proyectil
class Projectile {
    constructor(x, y, width, height, color) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color;
        this.speed = 7; // Velocidad del proyectil
    }

    draw(ctx) {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }

    update() {
        this.y -= this.speed;
    }
}

// Exporta las clases para que puedan ser importadas en otros archivos
export { Player, Enemy, Projectile };
