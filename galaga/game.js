// game.js

// Importa las clases desde el archivo classes.js
import { Player, Enemy, Projectile } from './classes.js';

// Obtiene el elemento canvas del HTML
const canvas = document.getElementById('gameCanvas');
// Obtiene el contexto 2D del canvas para dibujar
const ctx = canvas.getContext('2d');

// Ajusta el tamaño del canvas para que ocupe toda la ventana
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

// Instancia de la clase Player para crear al jugador
const player = new Player(canvas.width / 2 - 25, canvas.height - 60, 50, 50, 'white');

// Array para almacenar a los enemigos
const enemies = [];
for (let i = 0; i < 5; i++) {
    enemies.push(new Enemy(Math.random() * canvas.width, Math.random() * canvas.height - canvas.height, 50, 50, 'red'));
}

// Array para almacenar los proyectiles
const projectiles = [];

// Función principal del juego que actualiza y redibuja los elementos del canvas
function update() {
    // Limpia el canvas antes de dibujar
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // Dibuja al jugador
    player.draw(ctx);

    // Actualiza y dibuja a cada enemigo
    enemies.forEach((enemy, enemyIndex) => {
        enemy.update(canvas);
        enemy.draw(ctx);

        // Verifica colisiones entre proyectiles y enemigos
        projectiles.forEach((projectile, projectileIndex) => {
            if (projectile.x < enemy.x + enemy.width &&
                projectile.x + projectile.width > enemy.x &&
                projectile.y < enemy.y + enemy.height &&
                projectile.y + projectile.height > enemy.y) {
                // Elimina el enemigo y el proyectil si colisionan
                enemies.splice(enemyIndex, 1);
                projectiles.splice(projectileIndex, 1);
            }
        });
    });

    // Actualiza y dibuja cada proyectil
    projectiles.forEach((projectile, index) => {
        projectile.update();
        projectile.draw(ctx);

        // Elimina el proyectil si sale de la pantalla
        if (projectile.y + projectile.height < 0) {
            projectiles.splice(index, 1);
        }
    });

    // Llama a la función update nuevamente en el siguiente frame
    requestAnimationFrame(update);
}

// Maneja el evento de pulsar una tecla para mover al jugador y disparar proyectiles
function keyDownHandler(event) {
    if (event.key === 'ArrowLeft') {
        player.move('left');
    } else if (event.key === 'ArrowRight') {
        player.move('right');
    } else if (event.key === 'ArrowUp') {
        player.move('up');
    } else if (event.key === 'ArrowDown') {
        player.move('down');
    } else if (event.key === ' ') {
        // Crear un nuevo proyectil cuando se presiona la barra espaciadora
        const projectile = new Projectile(player.x + player.width / 2 - 2.5, player.y, 5, 10, 'white');
        projectiles.push(projectile);
    }
}

// Añade un evento para escuchar cuando se pulsa una tecla
document.addEventListener('keydown', keyDownHandler);

// Inicia el juego llamando a la función update por primera vez
update();
