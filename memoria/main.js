// inicializacion de variables
let tarjetasDestapadas = 0;
let tarjeta1 = null;
let tarjeta2 = null;
let primerResultado = null;
let segundoResultado = null;
let movimientos = 0;
let aciertos = 0;
let temporizador = false;
let timer = 3;
let timerInicial = timer;
let timepoRegresivoId = null;
// apuntando al doc html
let mostrarMovimientos = document.getElementById('movimientos');
let mostrarAciertos = document.getElementById('aciertos');
let mostrarTiempo =document.getElementById('t-restante');
//generacion de numeros aleatorios
let numeros = [1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8];
numeros = numeros.sort(()=>{return Math.random() -0.5});
console.log(numeros);
// funcion tiempo
function contarTiempo(){
    timepoRegresivoId = setInterval(()=>{
        timer--;
        mostrarTiempo.innerHTML  = `Tiempo ${timer} segundos`;
        if (timer == 0){
            // detenemos en 0 
            clearInterval(timepoRegresivoId);
            bloquearTarjetas();
        }
    },1000);
}
function bloquearTarjetas(){
    for (let i = 0 ; i<=15;i++){
        let tarjetaBloqueada = document.getElementById(i)
        tarjetaBloqueada.innerHTML = numeros[i];
        tarjetaBloqueada.disabled= true;
    }
}

//funcion primcipal
function destapar(id){
    if(temporizador == false){
        contarTiempo();
        temporizador = true;
    }
    tarjetasDestapadas++;
    console.log(tarjetasDestapadas);
    if(tarjetasDestapadas ==1){
        //mostramos primero numero
        tarjeta1 = document.getElementById(id);
        primerResultado = numeros[id]
        tarjeta1.innerHTML = primerResultado;
        //ahora deshabilitamos el boton para que el contador no siga sumando. minuto 29 aprox
        tarjeta1.disabled = true;
    }
    else if(tarjetasDestapadas == 2 ){
        //mostrar segundo numero
        tarjeta2 = document.getElementById(id);
        segundoResultado = numeros[id];
        tarjeta2.innerHTML = segundoResultado;
        // deshabilito el 2do boton 
        tarjeta2.disabled = true;
        // aumentamos la variable de movimientos en 1, lo hacemos cuando el jugador hace click en el segundo boton por las regolas del juego 
        movimientos++;
        mostrarMovimientos.innerHTML = `Movimientos: ${movimientos}`;
        if(primerResultado == segundoResultado){
            tarjetasDestapadas = 0 ;

            // aumento aciertos 
            aciertos++;
            mostrarAciertos.innerHTML = `Aciertos: ${aciertos}`;
            if(aciertos == 8){
                clearInterval(timepoRegresivoId);
                mostrarAciertos.innerHTML = `Aciertos: ${aciertos} !!`;
                mostrarTiempo.innerHTML = `Fantastico, solo te demoraste: ${timerInicial - timer}`;
                mostrarMovimientos.innerHTML = `movimientos ${movimientos}`;
            }
        }
        else{ 
            //mostramos momentaneamente los valores  ylos volvemos a atapar minuto 40 aprox
            setTimeout(()=>{
                tarjeta1.innerHTML = " ";
                tarjeta2.innerHTML = " ";
                tarjeta1.disabled = false ;
                tarjeta2.disabled = false;
                tarjetasDestapadas = false;
                },800);
        }
    }
}
 