// para importar una clase primero debemos exportarla, viniendo de java es medio raro...
export default class Model {
    constructor() {
        this.view = null;
        this.todos = [];
        this.currentId = 1;
    }
    setView(view){
        this.view = view ;
    }
    getTodos(){
        return this.todos;
    }
    findTodo(){
        return this.todos.findIndex((todo) => todo.id === id);
    }
    toggleCompleted(id){
        console.log(id);
        // const index = this.findTodo(id);
        // const todo = this.todos[index];
        // todo.completed = !todo.completed;
        // console.log(this.todos);
    }
    addTodo(title,description ){
        console.log(title,description);
        const todo = {
            id : this.currentId++,
            title ,
            description ,
            completed : false,
        }
        this.todos.push(todo);
        console.log(this.todos)
        // sino me equivoco la sigueinte linea de codigo es paa que no hay un aliasing, estonces creamos un objeto vacio y le asignamos las propedades de "todo"
        // return Object.assign({}, todo);
        // la siquiente liena es equiv a la anterior 
        return {...todo};

    }
    removeTodo (id){
        const index = this.findTodo(id);
        this.todos.splice(index,1);
    }
}