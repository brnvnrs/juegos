document.addEventListener("DOMContentLoaded" , function() {
    // primero vamos a obtener el valor de title y descripcion para despues añadir
    const title = document.getElementById("title");
    const description = document.getElementById("description");
    const table = document.getElementById("table");
    const alert = document.getElementById("alert");
    const btn = document.getElementById("add");
    let id=1;
    function removeToDo(id) {
      // console.log(id);
      document.getElementById(id).remove();
    }
    function addToDo(){
        if (title.value === "" || description.value === ""){
            // aca lo que tenemos es que usando jquery el div que tiene la id alert tinee el display en none(osea d-none) vamos a querer quitar eso
            alert.classList.remove("d-none");
            alert.innerText = "title and description are required" ;
            return;
        }
        alert.classList.add("d-none");
        const row = table.insertRow();
        row.setAttribute("id",id++);
        row.innerHTML = `
            
            <td >${title.value}</td>
            <td >${description.value}</td>
            <td class="text-center">
                <input type="checkbox">
              </td>
              <td class="text-right">
                <button class="btn btn-primary mb-1">
                  <i class="fa fa-pencil"></i>
                </button>
              </td>
        `;
        const removeBtn = document.createElement("button");
        removeBtn.classList.add('btn' , "btn-danger", "mb-1", "ml-1");
        removeBtn.innerHTML = '<i class="fa fa-trash"></i>';
        removeBtn.onclick = function(e){
          removeToDo(row.getAttribute("id"));
        };
        row.children[3].appendChild(removeBtn);


    }






    // si pongo btn.onclick = addToDo() lo que va a pasar es que btn.onclick va a tener el valor de la funcion, si es que retorna algo, lo que hay que hacer es btn.onclick = addToDo para que se ejecute esa funcion , me quedo en el min 29
    btn.onclick = addToDo;




})


