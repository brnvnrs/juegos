const nav = document.querySelector("#nav");
const abrir = document.querySelector("#abrir");
const cerrar = document.querySelector("#cerrar");

// ahora le agregamos un eveneto al abrir  apra que cuando le hagamos click nos muestre la nav bar

abrir.addEventListener("click" , () =>{
    nav.classList.add("visible");
})

cerrar.addEventListener("click" , () =>{
    nav.classList.remove("visible");
})