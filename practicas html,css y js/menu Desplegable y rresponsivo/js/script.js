// ejecutar funcion en el evento
document.getElementById("btn_open").addEventListener("click", open_close_menu);

// declaramos las variables 
var side_menu = document.getElementById("menu__side");
var btn_open= document.getElementById("btn_open");
var body = document.getElementById("body");


// evcento para mostrar o ocultar el menu

function open_close_menu(){
    body.classList.toggle("body_move");
    side_menu.classList.toggle("menu__side_move"); 
}
//  si el ancho de la pagina es menor a 760 px, se ocultará el menu al recargar la pagina 

if (window.innerWidth < 760){
    body.classList.add("body_move");
    side_menu.classList.add("menu__side_move")
}
// haciendo elmenu respnosivo(adaptable)



window.addEventListener("resize" , function(){
    if(this.window.innerWidth > 760){
        body.classList.remove("body_move");
        side_menu.classList.remove("menu__side_move");
    }
    if(this.window.innerWidth < 760){
        body.classList.add("body_move");
        side_menu.classList.add("menu__side_move");
    }
});